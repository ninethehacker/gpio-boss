#! /usr/bin/env python3

# GPIO Boss
# graphical utility for manually controlling gpio on raspberry pi

import RPi.GPIO as GPIO
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

pin_label = ["no zero pin",
    "3v3", "5v",
    "GPIO2", "5v",
    "GPIO3", "GROUND",
    "GPIO4", "GPIO14",
    "GROUND", "GPIO15",
    "GPIO17", "GPIO18",
    "GPIO27", "GROUND",
    "GPIO22", "GPIO23",
    "3v3", "GPIO24",
    "GPIO10", "GROUND",
    "GPIO9", "GPIO25",
    "GPIO11", "GPIO8",
    "GROUND", "GPIO7",
    "ZEROBOI", "ONEBOI",
    "GPIO5", "GROUND",
    "GPIO6", "GPIO12",
    "GPIO13", "GROUND",
    "GPIO19", "GPIO16",
    "GPIO26", "GPIO20",
    "GROUND", "GPIO21"]

class PinBoi(Gtk.Box):
    pin = 0
    pin_input = False
    control = None
    
    def __init__(self, pin):
        Gtk.Box.__init__(self)
        self.pin = pin
        
        config = Gtk.Button(label=pin)
        config.connect("clicked", self.change_mode)

        label = Gtk.Label(label=pin_label[pin])
        
        if pin%2==0:
            self.pack_start(config, False, True, 0)
            self.pack_start(label, False, True, 0)
        else:
            self.pack_end(config, False, True, 0)
            self.pack_end(label, False, True, 0)
        
        if "GPIO" in pin_label[pin]:
            self.configure_pin(pin)
            switch = Gtk.Switch()
            switch.connect("state-set", self.on_switch_toggled)
            if pin%2==1:
                self.pack_start(switch, False, False, 0)
            else:
                self.pack_end(switch, False, False, 0)

    def configure_pin(self, pin):
        GPIO.setup(pin, GPIO.OUT)

    def on_switch_toggled(self, widget, state):
        GPIO.output(self.pin, state)

    def change_mode(self, widget):
        self.pin_input = not self.pin_input
        print("pin: %s, mode %s" % (self.pin, self.pin_input))

class GPIOBoss(Gtk.ApplicationWindow):
    def __init__(self):
        Gtk.ApplicationWindow.__init__(self)
        
        GPIO.setmode(GPIO.BOARD)
        
        layout = Gtk.Grid()        
        for pin in range(1, 41):
            boi = PinBoi(pin)
            layout.attach(boi, pin%2==0, (pin/2)-(pin%2==0), 1, 1)
        
        self.set_title("GPIO Boss")
        self.add(layout)
        self.connect("destroy", self.on_quit)
        self.show_all()
    
    def on_quit(self, widget):
        GPIO.cleanup()
        Gtk.main_quit()

window = GPIOBoss()
Gtk.main()